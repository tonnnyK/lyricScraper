# Finds the taglib library
#
# Defines the variables:
#
#   Taglib_FOUND
#   Taglib_INCLUDE_DIRS
#
# and the imported target:
#
#   Taglib::Taglib

find_package(PkgConfig)
pkg_check_modules(PC_Taglib REQUIRED taglib)

find_path(Taglib_INCLUDE_DIR
    NAMES taglib.h
    PATHS ${PC_Taglib_INCLUDE_DIRS}
    PATH_SUFFIXES taglib
)
set(TAGLIB_VERSION ${PC_Taglib_VERSION})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Taglib
    REQUIRED_VARS Taglib_INCLUDE_DIR
    VERSION_VAR Taglib_VERSION
)

if(Taglib_FOUND)
    set(Taglib_INCLUDE_DIRS ${Taglib_INCLUDE_DIR})
endif()

if(Taglib_FOUND AND NOT TARGET Taglib::Taglib)
    add_library(Taglib::Taglib INTERFACE IMPORTED)
    set_target_properties(Taglib::Taglib PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${Taglib_INCLUDE_DIR}"
    )
endif()

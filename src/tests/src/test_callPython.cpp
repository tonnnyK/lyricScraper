#include "../../../extern/catch/single_include/catch2/catch.hpp"
#include "python/include/callPython.hpp"
#include "exception/include/http_error.hpp"

TEST_CASE("callPython on python scraper modules") {

    using namespace lyricScraper;
    SECTION("Module name or function name not given") {
        std::vector<std::string> args{
            "outkast",
            "liberation"
        };

        REQUIRE_THROWS_AS(callPython("", "geniusScraper", args),
                          std::invalid_argument);
        REQUIRE_THROWS_AS(callPython("geniusScraper", "", args),
                          std::invalid_argument);
    }

    SECTION("Wrong number of arguments given to python function") {
        REQUIRE_THROWS_AS(callPython("geniusScraper",
                                     "geniusScraper",
                                     std::vector<std::string>{}),
                          std::invalid_argument);

        std::vector<std::string> args{
            "hello",
            "world",
            "extra"
        };
        REQUIRE_THROWS_AS(callPython("geniusScraper",
                                     "geniusScraper",
                                     args),
                          std::invalid_argument);
    }

    SECTION("Http error") {
        std::vector<std::string> args{
            "outkast",
            "non existent song"
        };

        REQUIRE_THROWS_AS(callPython("geniusScraper", "geniusScraper",
                                     args),
                          http_error);
        try {
            callPython("geniusScraper", "geniusScraper",
                       args);
        }

        catch (const http_error& e) {
            REQUIRE(e.code() == 404);
        }

        args = {
            "non existent artist",
            "liberation"
        };

        REQUIRE_THROWS_AS(callPython("geniusScraper", "geniusScraper",
                                     args),
                          http_error);
        try {
            callPython("geniusScraper", "geniusScraper",
                       args);
        }

        catch (const http_error& e) {
            REQUIRE(e.code() == 404);
        }
    }
}

#include <string>
#include <vector>
#include <iostream>
#include <filesystem>

#include "../../../extern/catch/single_include/catch2/catch.hpp"
#include "song/include/Song.hpp"
#include "song/include/selectSongs.hpp"
#include "exception/include/root_excluded.hpp"
#include "exception/include/invalid_file_type.hpp"

TEST_CASE("selectSongs from user input") {

    using namespace lyricScraper;
    namespace fs = std::filesystem;

    SECTION("no arguments given") {
        std::unordered_set<std::string> includeDirectories{};
        std::unordered_set<std::string> excludeDirectories{};
        auto selectedSongs = selectSongs(includeDirectories,
                                         excludeDirectories);
        REQUIRE(selectedSongs.empty());
    }

    SECTION("directories given but no excludes") {
        std::unordered_set<std::string> includeDirectories{
            "../testFiles/artist0"
        };

        std::unordered_set<std::string> excludeDirectories{};

        std::vector<Song> selectedSongs =
            selectSongs(includeDirectories,
                        excludeDirectories);
        REQUIRE(selectedSongs.size() == 27);
    }

    SECTION("root passed as an exclude") {
        std::unordered_set<std::string> includeDirectories{
            "../testFiles/artist0"
        };

        std::unordered_set<std::string> excludeDirectories{"/"};
        REQUIRE_THROWS_AS( selectSongs(includeDirectories,
                                       excludeDirectories),
                           root_excluded );
    }

    SECTION("54 files included, 18 excluded") {
        std::unordered_set<std::string> includeDirectories{
            "../testFiles/artist0",
            "../testFiles/artist1"
        };
        std::unordered_set<std::string> excludeDirectories{
            "../testFiles/artist0/artist0Album0",
            "../testFiles/artist0/artist0Album1"
        };

        auto selectedSongs = selectSongs(includeDirectories,
                                         excludeDirectories);
        REQUIRE(selectedSongs.size() == 36);
    }

    SECTION("ignores non music files") {
        std::unordered_set<std::string> includeDirectories{
            "../testFiles/artist2"
        };
        std::unordered_set<std::string> excludeDirectories{};

        auto selectedSongs = selectSongs(includeDirectories,
                                         excludeDirectories);
        REQUIRE(selectedSongs.size() == 27);
    }
}

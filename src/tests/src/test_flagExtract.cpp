#include <string>

#include "../../../extern/catch/single_include/catch2/catch.hpp"
#include "flag/include/flagExtract.hpp"

TEST_CASE("Extracting flags") {

    SECTION("one set of flags given") {
        std::vector<std::string> args1 {
            "-d"
        };

        auto flags = flagExtract::extract(args1);
        REQUIRE(flags.size() == 1);
        REQUIRE(flags.find('d') != flags.end());
    }

    SECTION("one set of flags and directory") {

        std::vector<std::string> args2 {
            "-dsf",
            "../music/directory/artist1"
            "../music/directory/artist2"
        };

        auto flags = flagExtract::extract(args2);
        REQUIRE(flags.size() == 3);
        REQUIRE(flags.find('d') != flags.end());
        REQUIRE(flags.find('s') != flags.end());
        REQUIRE(flags.find('f') != flags.end());
    }

    SECTION("no flags") {
        std::vector<std::string> args3 {
            "../music/directory/artist/song1.mp3"
            "../music/directory/artist/song2.mp3"
        };

        auto flags = flagExtract::extract(args3);
        REQUIRE(flags.empty());
    }

    SECTION("no user args") {
        std::vector<std::string> args4;
        std::unordered_set<char> flags = flagExtract::extract(args4);
        REQUIRE(flags.empty());
    }

    SECTION("flags before directory given and after") {
        std::vector<std::string> args5 {
            "-sda",
            "../music/directory",
            "../music/otherDirectory",
            "-x",
            "../music/directory/artist"
        };

        std::unordered_set<char> flags = flagExtract::extract(args5);
        REQUIRE(flags.size() == 4);
        REQUIRE(flags.find('s') != flags.end());
        REQUIRE(flags.find('d') != flags.end());
        REQUIRE(flags.find('a') != flags.end());
        REQUIRE(flags.find('x') != flags.end());
    }

    SECTION("flag are separated") {
        std::vector<std::string> args{
            "-s",
            "-d",
            "-a",
            "-x"
        };

        std::unordered_set<char> flags = flagExtract::extract(args);
        REQUIRE(flags.size() == 4);
        REQUIRE(flags.find('s') != flags.end());
        REQUIRE(flags.find('d') != flags.end());
        REQUIRE(flags.find('a') != flags.end());
        REQUIRE(flags.find('x') != flags.end());
    }
}

#include "../../../extern/catch/single_include/catch2/catch.hpp"
#include "inputHandler/include/InputHandler.hpp"
#include "exception/include/no_files_given.hpp"
#include "exception/include/invalid_file_type.hpp"

TEST_CASE("InputHandler reads user input from command line arguments") {

    using input_handler = lyricScraper::InputHandler;
    using namespace lyricScraper;

    SECTION("duplicate flags are silently ignored") {
        std::vector<std::string> args1{
            "-abd",
            "-sed",
            "../testFiles/artist0"
        };

        input_handler inputHandler1{args1};
        REQUIRE(inputHandler1.numFlags() == 5);

        std::vector<std::string> args2 {
            "-aaa",
            "../testFiles/artist0"
        };

        input_handler inputHandler2{args2};
        REQUIRE(inputHandler2.numFlags() == 1);
    }

    SECTION("no user args") {
        std::vector<std::string> arg;
        REQUIRE_THROWS_AS(input_handler{arg},
                          no_files_given);
    }

    SECTION("user flags, no files given") {
        std::vector<std::string> arg{
            "-af"
        };
        REQUIRE_THROWS_AS(input_handler{arg},
                          no_files_given);
    }

    SECTION("file and folder given") {
        std::vector<std::string> arg{
            "-de",
            "../testFiles/Csus.mp3",
            "../testFiles/artist1"
        };
        input_handler inputHandler{arg};
        auto includePaths = inputHandler.includePaths();
        REQUIRE(includePaths.size() == 2);
        REQUIRE(includePaths.find("../testFiles/Csus.mp3") !=
                includePaths.end());
        REQUIRE(includePaths.find("../testFiles/artist1") !=
                includePaths.end());
        REQUIRE(inputHandler.excludePaths().empty());
    }

    SECTION("exclude flag given, but no exclude files given") {
        std::vector<std::string> arg{
            "-de",
            "../testFiles/Csus.mp3",
            "../testFiles/artist1",
            "-x"
        };
        input_handler inputHandler{arg};
        auto includePaths = inputHandler.includePaths();
        REQUIRE(includePaths.size() == 2);
        REQUIRE(includePaths.find("../testFiles/Csus.mp3") !=
                includePaths.end());
        REQUIRE(includePaths.find("../testFiles/artist1") !=
                includePaths.end());
        REQUIRE(inputHandler.excludePaths().empty());
    }

    SECTION("exclude flag given") {
        std::vector<std::string> arg{
            "-de",
            "../testFiles/Csus.mp3",
            "../testFiles/artist1",
            "-x",
            "../testFiles/artist1/artist1Album0/artist1Album0Song0.mp3",
            "../testFiles/artist2",
            "../testFiles/artist1/artist1Album1"
        };
        input_handler inputHandler{arg};
        auto includePaths = inputHandler.includePaths();
        REQUIRE(includePaths.size() == 2);
        REQUIRE(includePaths.find("../testFiles/Csus.mp3") !=
                includePaths.end());
        REQUIRE(includePaths.find("../testFiles/artist1") !=
                includePaths.end());

        auto excludePaths = inputHandler.excludePaths();
        REQUIRE(excludePaths.size() == 3);
        REQUIRE(excludePaths.find("../testFiles/artist1/artist1Album0/artist1Album0Song0.mp3") !=
                excludePaths.end());
        REQUIRE(excludePaths.find("../testFiles/artist2") !=
                excludePaths.end());
        REQUIRE(excludePaths.find("../testFiles/artist1/artist1Album1") !=
                excludePaths.end());
    }

    SECTION("user provided files, but they weren't music files") {
        std::vector<std::string> arg{
            "../testFiles/notsong.txt"
        };
        REQUIRE_THROWS_AS(input_handler{arg}, invalid_file_type);
    }
}

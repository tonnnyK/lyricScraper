#ifndef INPUT_HANDLER_HPP
#define INPUT_HANDLER_HPP

#include <unordered_set>
#include <vector>
#include <string>
#include "flag/include/Flag.hpp"

namespace lyricScraper {

    class InputHandler {

        public:
            InputHandler(const std::vector<std::string>&);
            const std::unordered_set<std::string>& includePaths() const;
            const std::unordered_set<std::string>& excludePaths() const;
            bool flagGiven(const Flag&) const;
            std::unordered_set<std::string>::size_type numFlags() const;

        private:
            std::unordered_set<Flag> flags_;

            std::unordered_set<std::string> includePaths_;
            std::unordered_set<std::string> excludePaths_;

            void addPaths(const std::vector<std::string>&);
    };
}

#endif

#include <vector>
#include <algorithm>

#include "inputHandler/include/InputHandler.hpp"
#include "exception/include/no_files_given.hpp"
#include "exception/include/invalid_file_type.hpp"
#include "flag/include/flagExtract.hpp"
#include "typeCheck/include/isMusicFile.hpp"

// helpers
namespace {
    bool argIsFlag(const std::string&);
    bool argIsExcludeFlag(const std::string&);
    void checkFileIsValid(const std::string&);
}

namespace lyricScraper {

    // args: ./lyricScraper [-options] [files...] [-x files...]
    InputHandler::InputHandler(const std::vector<std::string>& args)
        : flags_{flagExtract::extract(args)} {

        addPaths(args);
        if (includePaths_.empty()) {
            throw no_files_given{"no files given"};
        }
    }

    // Adds directories from user input.
    // If exclude flag was given, all
    // directories after the flag will be excluded
    void InputHandler::addPaths(const std::vector<std::string>& args) {

        bool excludeFlagGiven  = false;
        for (auto arg : args) {
            if (argIsExcludeFlag(arg)) {
                excludeFlagGiven = true;
            }
            else if (!argIsFlag(arg)) {
                checkFileIsValid(arg);
                if (excludeFlagGiven) {
                    excludePaths_.insert(arg);
                }
                else {
                    includePaths_.insert(arg);
                }
            }
        }
    }

    const std::unordered_set<std::string>&
        InputHandler::includePaths() const {

        return includePaths_;
    }

    const std::unordered_set<std::string>&
        InputHandler::excludePaths() const {

        return excludePaths_;
    }

    // returns true if the flag f was given
    bool InputHandler::flagGiven(const Flag& f) const {

        if (std::find(flags_.begin(), flags_.end(), f) !=
            flags_.end()) {
            return true;
        }
        else {
            return false;
        }
    }

    std::unordered_set<std::string>::size_type
        InputHandler::numFlags() const {

        return flags_.size();
    }
}

// helper functions
namespace {
    // returns true if given argument
    // is a flag/s i.e. preceded by -
    bool argIsFlag(const std::string& arg) {

        return (arg.front() == '-' ? true : false);
    }

    // returns true if given argument contains
    // an exclude flag e.g. -x
    bool argIsExcludeFlag(const std::string& arg) {

        if (!argIsFlag(arg)) return false;
        const auto NOT_FOUND = std::string::npos;
        if (arg.find('x') == NOT_FOUND) {
            return false;
        }
        return true;
    }

    // throws exception if file isn't valid for
    // input, does nothing if valid
    void checkFileIsValid(const std::string& file) {

        namespace fs = std::filesystem;

        if (!fs::exists(file)) {
            throw fs::filesystem_error{std::string{"doesn't exist"},
                                       fs::path{file},
                                       std::error_code{}};
        }

        // user must give a folder or if a file,
        // must be a music file
        if (std::filesystem::is_regular_file(file) &&
            !isMusicFile(file)) {
            throw lyricScraper::invalid_file_type{"not a music file",
                                                  file};
        }
    }
}

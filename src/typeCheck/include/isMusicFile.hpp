#ifndef IS_MUSIC_FILE_HPP
#define IS_MUSIC_FILE_HPP

#include <filesystem>

bool isMusicFile(const std::filesystem::path&);
#endif

#include "typeCheck/include/isMusicFile.hpp"

// returns true if path is an .mp3 or .flac file
bool isMusicFile(const std::filesystem::path& p) {

    namespace fs = std::filesystem;
    if (!fs::exists(p)) {
        throw fs::filesystem_error{std::string{"doesn't exist"},
                                   fs::path{p},
                                   std::error_code{}};
    }

    if (fs::is_regular_file(p)) {
        auto fileExtension = fs::path{p}.extension();
        if (fileExtension.string() == ".mp3" ||
            fileExtension.string() == ".flac") {
            return true;
        }
    }
    return false;
}

/*
 * Usage:
 * lyricScraper [-options] files... [-x files...]
 *
 * Options:
 *
 * -x:
 *  optional list of files to exclude
 */
#include <vector>
#include <string>
#include <iostream>

#include "inputHandler/include/InputHandler.hpp"
#include "song/include/SongReader.hpp"
#include "scraper/include/scrapeLyrics.hpp"
#include "exception/include/invalid_file_type.hpp"
#include "exception/include/invalid_input.hpp"

namespace {

    const std::vector<std::string>
          argsToStrings(const int argc, char* argv[]);
    void printHelp();
}

int main(int argc, char* argv[]) {

    try {
        lyricScraper::InputHandler inputHandler{argsToStrings(argc, argv)};
        lyricScraper::SongReader songReader{inputHandler};
        lyricScraper::scrapeLyrics(songReader.songsBegin(), songReader.songsEnd());
    }

    catch (const lyricScraper::invalid_input& e) {
        std::cerr << "Exception: " << e.what() << "\n";
        printHelp();
    }

    catch (const std::exception& e) {
        std::cerr << "Exception: " << e.what() << "\n";
    }

    catch (...) {
        std::cerr << "Unknown exception\n";
    }
}

namespace {

    // Converts c style command line arguments to c++
    // collection of strings
    const std::vector<std::string>
        argsToStrings(const int argc, char* argv[]) {

        std::vector<std::string> userArgs;
        const int USER_ARG_START_INDEX = 1;
        for (int i = USER_ARG_START_INDEX; i < argc; ++i) {
            userArgs.emplace_back(argv[i]);
        }

        return userArgs;
    }

    void printHelp() {

        std::cout << "\e[1mUsage:\e[0m\n\n";
        std::cout << "lyricScraper [-options] files... [-x files...]\n";
        std::cout << "\e[1m-x:\e[0m Excludes files following this flag from being scraped\n";
        std::cout << "\e[1mOptions:\e\n";
        std::cout << "\e[1m-s:\e[0m Directories in the songs directory\n";
        std::cout << "\e[1m-h:\e[0m Help\n";
    }
}

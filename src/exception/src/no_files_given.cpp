#include "exception/include/no_files_given.hpp"

namespace lyricScraper {

    no_files_given::no_files_given(const char* what_arg)
        : invalid_input{what_arg} {}
}

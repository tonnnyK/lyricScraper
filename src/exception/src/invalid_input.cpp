#include "exception/include/invalid_input.hpp"

namespace lyricScraper {

    invalid_input::invalid_input(const char* what_arg)
        : std::runtime_error{what_arg} {}
}

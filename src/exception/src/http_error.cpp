#include "exception/include/http_error.hpp"

namespace lyricScraper {

    http_error::http_error(const char* what_arg, const int errorCode)
        : std::runtime_error(what_arg), httpErrorCode_{errorCode} {}

    const int http_error::code() const {

        return httpErrorCode_;
    }
}

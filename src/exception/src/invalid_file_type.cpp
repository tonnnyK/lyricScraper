#include "exception/include/invalid_file_type.hpp"

namespace lyricScraper {

    invalid_file_type::invalid_file_type(const char* what_arg,
                                         const std::string& path)
        : std::runtime_error{what_arg}, path_{path} {}

    const std::string& invalid_file_type::path() const {
        return path_;
    }
}

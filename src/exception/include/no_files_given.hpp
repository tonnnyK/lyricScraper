#ifndef NO_FILES_GIVEN_HPP
#define NO_FILES_GIVEN_HPP

#include "invalid_input.hpp"

namespace lyricScraper {

    class no_files_given
        : public invalid_input {

        public:
            explicit no_files_given(const char* what_arg);
    };
}

#endif

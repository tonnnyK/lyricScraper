#ifndef INVALID_ARGUMENTS_HPP
#define INVALID_ARGUMENTS_HPP

#include <stdexcept>

namespace lyricScraper {
    
    class invalid_input
        : public std::runtime_error {
        
        public:
            explicit invalid_input(const char* what_arg);   
    };
}

#endif

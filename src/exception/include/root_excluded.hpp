#ifndef ROOT_EXCLUDED_HPP
#define ROOT_EXCLUDED_HPP

#include "invalid_input.hpp"

namespace lyricScraper {

    class root_excluded
        : public invalid_input {

        public:
            explicit root_excluded(const char* what_arg);
    };
}

#endif

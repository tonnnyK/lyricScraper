#ifndef DUPLICATE_FLAGS_HPP
#define DUPLICATE_FLAGS_HPP

#include "invalid_input.hpp"

namespace lyricScraper {

    class duplicate_flags
        : public invalid_input {

        public:
            explicit duplicate_flags(const char* what_arg);
    };
}

#endif

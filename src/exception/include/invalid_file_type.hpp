#ifndef INVALID_FILE_TYPE_HPP
#define INVALID_FILE_TYPE_HPP

#include <stdexcept>
#include <string>

namespace lyricScraper {

    class invalid_file_type
        : public std::runtime_error {

        public:
            explicit invalid_file_type(const char* what_arg,
                                       const std::string& path);
            // returns the path that has the wrong type
            const std::string& path() const;

        private:
            std::string path_;
    };
}

#endif

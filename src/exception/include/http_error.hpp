#ifndef HTTP_ERROR_HPP
#define HTTP_ERROR_HPP

#include <stdexcept>

namespace lyricScraper {

    class http_error
        : public std::runtime_error {

        public:
            explicit http_error(const char* what_arg, const int);
            // returns http error code
            const int code() const;

        private:
            const int httpErrorCode_;
    };
}

#endif

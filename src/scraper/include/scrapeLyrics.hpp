#ifndef SCRAPE_HPP
#define SCRAPE_HPP

#include "song/include/SongReader.hpp"

namespace lyricScraper {

    void scrapeLyrics(Song&);
    void scrapeLyrics(SongReader::iterator,
                      SongReader::iterator);
}
#endif

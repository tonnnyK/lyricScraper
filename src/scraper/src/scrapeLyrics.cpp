#include <string>
#include <vector>
#include <taglib/mpegfile.h>
#include <taglib/id3v2tag.h>
#include <taglib/unsynchronizedlyricsframe.h>

#include "scraper/include/scrapeLyrics.hpp"
#include "python/include/callPython.hpp"
#include "exception/include/http_error.hpp"

// helpers
namespace {
    bool lyricsNotFound(const lyricScraper::http_error&);
    void writeLyrics(const std::string&, const std::string&);
}

namespace lyricScraper {

    // scrapes lyrics for song and writes to the song file
    void scrapeLyrics(Song& song) {

        try {
            const std::vector<std::string> args{song.artist(),
                                                song.songName()};
            const std::string lyrics =
                callPython("geniusScraper",
                           "geniusScraper",
                           args);
            writeLyrics(song.path(), lyrics);
        }

        // TODO: use search function of the websites
        // in case of a 404 error
        catch (const http_error& e) {
            if (lyricsNotFound(e)) {
                throw std::runtime_error{"not found"};
            }
        }

        catch (...) {
            throw;
        }
    }

    // scrapes lyrics from songs in songReader and writes to the
    // song files
    void scrapeLyrics(SongReader::iterator begin,
                      SongReader::iterator end) {

        for (auto songsIt = begin; songsIt != end; ++songsIt) {
            scrapeLyrics(*songsIt);
        }
    }
}

namespace {

    // returns true if exception was thrown because
    // the lyrics weren't found i.e. HTTP code 404
    bool lyricsNotFound(const lyricScraper::http_error& e) {

        return (e.code() == 404);
    }

    // write lyrics to file
    void writeLyrics(const std::string& filepath,
                     const std::string& lyrics) {

        TagLib::MPEG::File file(filepath.c_str());

        // get lyric frame from file's id3v2 tag
        auto lyricFrame = file.ID3v2Tag(true)->frameListMap()["USLT"];

        // taglib documentation recommends using addFrame and removeFrame
        // to manipulate the frames instead of doing it directly
        if (!lyricFrame.isEmpty()) {
            file.ID3v2Tag()->removeFrames("USLT");
        }

        // add new frame with scraped lyrics
        using LyricsFrame  = TagLib::ID3v2::UnsynchronizedLyricsFrame;
        LyricsFrame* newLyrics =
            new TagLib::ID3v2::UnsynchronizedLyricsFrame{TagLib::String::UTF8};
        newLyrics->setText(lyrics);
        file.ID3v2Tag()->addFrame(newLyrics);
        file.save();
    }
}

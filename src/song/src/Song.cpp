#include "song/include/Song.hpp"

using namespace lyricScraper;

Song::Song(const std::string& artist,
           const std::string& song,
           const std::string& path)
 : artist_{artist}, songName_{song}, path_{path} {}

const std::string& Song::artist() const {

    return artist_;
}

const std::string& Song::songName() const {

    return songName_;
}

const std::string& Song::path() const {

    return path_;
}

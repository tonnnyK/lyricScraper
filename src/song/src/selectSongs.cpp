#include <algorithm>
#include <filesystem>
#include <taglib/fileref.h>

#include "song/include/selectSongs.hpp"
#include "exception/include/root_excluded.hpp"
#include "exception/include/invalid_file_type.hpp"
#include "typeCheck/include/isMusicFile.hpp"

// helpers
namespace {

    void checkFileExists(const std::string&);
    lyricScraper::Song readFromMusicFile(const std::filesystem::path&);
    bool pathIsExcluded(const std::string&,
                        const std::unordered_set<std::string>&);
    bool isSubpath(const std::string&, const std::string&);
    void addSong(const std::filesystem::path&,
                 std::vector<lyricScraper::Song>&);
}

namespace lyricScraper {

    // Returns a vector of individual songs from the directories
    // the user provided, excluding those from the excludePaths
    std::vector<Song>
        selectSongs(const std::unordered_set<std::string>& includePaths,
                    const std::unordered_set<std::string>& excludePaths) {

        std::vector<lyricScraper::Song> selectedSongs;
        namespace fs = std::filesystem;

        for (auto& includePath: includePaths) {
            auto pathIterator = fs::recursive_directory_iterator(includePath);
            for (auto directoryEntry = fs::begin(pathIterator);
                 directoryEntry != fs::end(pathIterator);
                 ++directoryEntry) {
                // ignore excluded directories
                if (pathIsExcluded(directoryEntry->path(), excludePaths) &&
                    fs::is_directory(directoryEntry->path())) {
                    directoryEntry.disable_recursion_pending();
                }
                else if (!fs::is_directory(directoryEntry->path())) {
                    addSong(*directoryEntry, selectedSongs);
                }
            }
        }
        return selectedSongs;
    }
}

// helpers
namespace {
    void checkFileExists(const std::string& file) {

        namespace fs = std::filesystem;
        if (!fs::exists(file)) {
            throw fs::filesystem_error{std::string{"doesn't exist"},
                                       fs::path{file},
                                       std::error_code{}};
        }
    }
    /*
     * Read the artist and title tags from the given filename
     * and return as a Song object
     */
    lyricScraper::Song
        readFromMusicFile(const std::filesystem::path& filename) {

        checkFileExists(filename);
        if (!isMusicFile(filename)) {
            throw lyricScraper::invalid_file_type{"not a music file",
                                                  filename};
        }
        namespace fs = std::filesystem;

        TagLib::FileRef songFile{filename.c_str()};
        std::string artist{(songFile.tag()->artist()).to8Bit(true)};
        std::string title{(songFile.tag()->title()).to8Bit(true)};
        return lyricScraper::Song{artist, title, filename};
    }

    bool pathIsRoot(const std::filesystem::path& path) {
        return !path.has_relative_path();
    }

    // returns true if lhs is contained within rhs or is
    // the same
    bool isSubpath(const std::string&lhs,
                   const std::string&rhs) {

        checkFileExists(lhs);
        namespace fs = std::filesystem;
        fs::path lhsCanonical = fs::canonical(lhs);
        checkFileExists(rhs);

        while (!pathIsRoot(lhsCanonical)) {
            if (fs::equivalent(lhsCanonical, rhs)) {
                return true;
            }
            else {
                lhsCanonical = lhsCanonical.parent_path();
            }
        }
        return false;
    }

    // Returns position of exclude path if
    // path or its parents is among the excludePaths.
    // returns end iterator if not
    bool pathIsExcluded(const std::string& path,
                        const std::unordered_set<std::string>& excludePaths) {

        namespace fs = std::filesystem;
        auto checkIfExcludedLambda = [&path](
                const std::string& excludePath) -> bool {
            // root being excluded would exclude everything!
            if (pathIsRoot(excludePath)) {
                throw lyricScraper::root_excluded{"root was excluded"};
            }
            return isSubpath(path, excludePath);
        };

        auto result = std::find_if(excludePaths.begin(),
                                   excludePaths.end(),
                                   checkIfExcludedLambda);
        return (result == excludePaths.end() ? false : true);
    }

    // Creates a new song from path and adds it to the back
    // of selectedSongs if path is a music file
    void addSong(const std::filesystem::path& path,
                 std::vector<lyricScraper::Song>& selectedSongs) {

        namespace fs = std::filesystem;
        if (fs::is_regular_file(path) && isMusicFile(path)) {
            selectedSongs.emplace_back(
                readFromMusicFile(path)
            );
        }
    }
}

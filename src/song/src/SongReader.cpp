#include "song/include/SongReader.hpp"
#include "song/include/selectSongs.hpp"

namespace lyricScraper {

    SongReader::
        SongReader(const InputHandler& inputHandler)
            : selectedSongs_{selectSongs(inputHandler.includePaths(),
                                         inputHandler.excludePaths())}
            {}

    SongReader::const_iterator
        SongReader::songsBegin() const noexcept {

        return selectedSongs_.cbegin();
    }

    SongReader::const_iterator
        SongReader::songsEnd() const noexcept {

        return selectedSongs_.cend();
    }

    SongReader::iterator
        SongReader::songsBegin() noexcept {

        return selectedSongs_.begin();
    }

    SongReader::iterator
        SongReader::songsEnd() noexcept {

        return selectedSongs_.end();
    }
}

#ifndef SONG_HPP
#define SONG_HPP

#include <string>

namespace lyricScraper {

    class Song {

        public:
            Song(const std::string& artist,
                 const std::string& songName,
                 const std::string& filename);

            const std::string& artist() const;
            const std::string& songName() const;
            const std::string& path() const;

        private:
            std::string artist_;
            std::string songName_;
            std::string path_;
    };
}

#endif

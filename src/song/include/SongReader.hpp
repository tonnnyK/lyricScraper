#ifndef IN_HANDLER_HPP
#define IN_HANDLER_HPP

#include <vector>

#include "Song.hpp"
#include "inputHandler/include/InputHandler.hpp"

namespace lyricScraper {

    class SongReader {

        public:
            SongReader(const InputHandler&);

            typedef std::vector<Song>::const_iterator const_iterator;
            const_iterator songsBegin() const noexcept;
            const_iterator songsEnd() const noexcept;

            typedef std::vector<Song>::iterator iterator;
            iterator songsBegin() noexcept;
            iterator songsEnd() noexcept;


        private:
            std::vector<Song> selectedSongs_;
        };
}

#endif

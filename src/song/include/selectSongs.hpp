#ifndef SELECT_SONGS_HPP
#define SELECT_SONGS_HPP

#include <unordered_set>
#include <vector>
#include <string>

#include "Song.hpp"

namespace lyricScraper {
    // Returns a vector of individual songs based on
    // includePaths.
    // Songs that are in excludePaths will be ignored.
    std::vector<Song> selectSongs(const std::unordered_set<std::string>& includePaths,
                                  const std::unordered_set<std::string>& excludePaths);
}
#endif

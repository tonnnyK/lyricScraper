#include "flagExtract.hpp"

namespace {
    bool argIsFlag(const std::string& arg) {

        return (arg.front() == '-') ? true : false;
    }
}
// from user input, returns a vector of flags based on the arguments
// supplied
// returns an empty vector if no flags given
namespace flagExtract {

    std::unordered_set<Flag> extract(
            const std::vector<std::string>& args) noexcept {

        if (args.empty()) {
            return std::unordered_set<Flag>{};
        }

        std::unordered_set<Flag> flagsSet;

        for (auto arg : args) {
            if (!argIsFlag(arg)) {
                continue;
            }

            // skip '-' character indicating flags
            auto argCharIt = arg.begin();
            ++argCharIt;
            for (; argCharIt != arg.end(); ++argCharIt) {
                flagsSet.insert(*argCharIt);
            }
        }

        return flagsSet;
    }
}

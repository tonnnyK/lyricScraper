#ifndef FLAG_EXTRACT_HPP
#define FLAG_EXTRACT_HPP

#include <unordered_set>
#include <vector>
#include <string>
#include "Flag.hpp"

namespace flagExtract {

    // from user input, returns a set of flags based on the arguments
    // supplied
    // returns an empty set if no flags given
    std::unordered_set<Flag> extract(const std::vector<std::string>& args) noexcept;
}

#endif

#ifndef CALL_PYTHON_HPP
#define CALL_PYTHON_HPP

#include <string>
#include <vector>

namespace lyricScraper {
    // calls and returns result of a python function that takes
    // in string arguments and returns a string
    std::string callPython(const std::string&,
                           const std::string&,
                           const std::vector<std::string>&);
}
#endif

#!/usr/bin/env python3

import requests, re
from bs4 import BeautifulSoup

def geniusScraper(artist, songName):
    artist = replaceWhiteSpace(artist)
    songName = replaceWhiteSpace(songName)

    url = 'https://genius.com/' + artist + '-' + songName + '-lyrics'
    res = requests.get(url)

    # throws exception if response code not ok
    if res.status_code != requests.codes.ok:
        res.raise_for_status()

    soup = bs4.BeautifulSoup(res.text, "html.parser")
    lyrics = soup.select('.lyrics p')

    return lyrics[0].getText()

# replaces white space with - to prepare for url
def replaceWhiteSpace(string):
    whiteSpaceRegex = re.compile(' +')
    return whiteSpaceRegex.sub('-', string)
#def geniusScraperWithSearch()
#https://genius.com/search?q=

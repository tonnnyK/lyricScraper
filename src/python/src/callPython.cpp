#include <Python.h>
#include <stdexcept>
#include <regex>

#include "callPython.hpp"
#include "exception/include/http_error.hpp"

// helpers
namespace {
    PyObject* importAndLoadModule(const std::string&);
    PyObject* callFunction(PyObject* const, const std::string&,
                           const std::vector<std::string>&);
    bool isAPythonFunction(PyObject* const);
    PyObject* buildArgumentTuple(const std::vector<std::string>&);
    std::string pyObjectToString(PyObject* const);
    int extractHTTPCode(const std::string&);
}

// calls and returns result of a python function that takes
// EXACTLY 2 string arguments and returns a string
// throws http_error if the called python function threw a http error
std::string lyricScraper::callPython(const std::string& moduleName,
                                     const std::string& functionName,
                                     const std::vector<std::string>& args) {

    if (moduleName.empty() || functionName.empty()) {
        throw std::invalid_argument{"nothing given"};
    }

    if (args.size() != 2) {
        throw std::invalid_argument{"requires exactly 2 arguments"};
    }

    Py_Initialize();
    PyObject* sys_path = PySys_GetObject("path");
    PyList_Append(sys_path, PyUnicode_FromString("."));
    //PySys_SetObject("path", sys_path);

    PyObject* pythonModule = importAndLoadModule(moduleName);
    std::string result;

    try {
        PyObject* pyReturnValue = callFunction(pythonModule,
                                               functionName, args);
        if (PyErr_Occurred() != NULL) {
            PyObject *ptype, *pvalue, *ptraceback;
            PyErr_Fetch(&ptype, &pvalue, &ptraceback);
            std::string errorMessage{pyObjectToString(pvalue)};
            Py_XDECREF(pyReturnValue);

            // call was successful but there was a http
            // exception. rethrow for c++
            int errorCode = extractHTTPCode(errorMessage);
            throw lyricScraper::http_error{"http error received",
                                            errorCode};
        }

        else {
            result = pyObjectToString(pyReturnValue);
            Py_DECREF(pyReturnValue);
        }

    }

    catch (...) {
        Py_DECREF(pythonModule);
        throw;
    }

    Py_DECREF(pythonModule);
    Py_FinalizeEx();
    return result;
}

// helpers

namespace {

    // imports and loads module name if it exists.
    // returns pointer to module, will never return NULL
    PyObject* importAndLoadModule(const std::string& moduleName) {

        PyObject* pythonName = PyUnicode_DecodeFSDefault(moduleName.c_str());
        if (pythonName == NULL) {
            throw std::runtime_error{"failed to decode module name " + moduleName};
        }

        PyObject* pythonModule = PyImport_Import(pythonName);
        Py_DECREF(pythonName);

        if (pythonModule == NULL) {
            throw std::runtime_error{"failed to load module " + moduleName};
        }

        return pythonModule;
    }

    // calls function and returns its return value
    // as a PyObject
    PyObject* callFunction(PyObject* const pythonModule,
                           const std::string& functionName,
                           const std::vector<std::string>& args) {

        PyObject* pythonFunction =
            PyObject_GetAttrString(pythonModule,
                                   functionName.c_str());

        if (!isAPythonFunction(pythonFunction)) {
            throw std::runtime_error{"can't find function: " + functionName};
        }

        PyObject* pythonArgs = buildArgumentTuple(args);
        if (pythonArgs == NULL) {
            Py_DECREF(pythonFunction);
            throw std::invalid_argument{"failed to convert args"};
        }

        PyObject* pyReturnValue =
            PyObject_CallObject(pythonFunction, pythonArgs);
        Py_DECREF(pythonArgs);
        Py_DECREF(pythonFunction);

        return pyReturnValue;
    }

    // returns true if f is a callable python function
    bool isAPythonFunction(PyObject* const f) {

        if (f != NULL && PyCallable_Check(f)) {
            return true;
        }

        else return false;
    }

    // returns a pyobject that is a tuple of the arguments
    PyObject* buildArgumentTuple(const std::vector<std::string>& args) {

        PyObject* pythonArgs = PyTuple_New(args.size());
        if (pythonArgs == NULL) {
            throw std::runtime_error{"failed to create python tuple"};
        }
        unsigned int i = 0;

        // convert c++ strings to python strings
        for (auto arg : args) {
            PyObject* pyString = PyUnicode_FromString(arg.c_str());
            if (pyString == NULL) {
                return NULL;
            }
            /* pValue reference stolen here: */
            PyTuple_SetItem(pythonArgs, i, pyString);
            ++i;
        }

        return pythonArgs;
    }

    // returns a c++ string representation of a pyobject
    std::string pyObjectToString(PyObject* const obj) {

        PyObject* pyObjectRepresentation = PyObject_Str(obj);
        if (pyObjectRepresentation == NULL) {
            throw std::runtime_error{
                "PyObject_Repr call failed on return value"
            };
        }
        PyObject* str = PyUnicode_AsEncodedString(pyObjectRepresentation,
                                                  "utf-8", "~E~");
        if (str == NULL) {
            Py_DECREF(pyObjectRepresentation);
            throw std::runtime_error{
                "PyUnicode_AsEncodedString call failed"
            };
        }
        std::string result{PyBytes_AS_STRING(str)};
        Py_DECREF(pyObjectRepresentation);
        Py_DECREF(str);
        return result;
    }

    // extracts and returns http error code from a string
    // in the form of
    // HTTPError('404 Client Error:...
    // only call when errorMessage is in this format
    int extractHTTPCode(const std::string& errorMessage) {

        std::regex codeRegex{"[0-9]{3}"};
        auto codeMatch = std::sregex_iterator{errorMessage.cbegin(),
                                             errorMessage.cend(),
                                             codeRegex};
        return std::stoi(codeMatch->str());
    }
}



# Lyric Scraper 

A CLI based tool that fetches lyrics from the internet and embeds them into
song files.

## Getting Started

### Prerequisites


```
TagLib
Python3
    - Beautiful Soup4
    - Requests
CMake minimum version 3.8
```

Make sure your python path is set to 

### Installing

Create a build directory to store the generated files by Cmake.
```
mkdir build
cd build/
cmake ..
```

To build the binary run `cmake --build . --target lyricScraper`
```
% cmake --build . --target lyricScraper
Scanning dependencies of target exception_lib
[  4%] Building CXX object src/exception/CMakeFiles/exception_lib.dir/src/duplicate_flags.cpp.o
[  8%] Building CXX object src/exception/CMakeFiles/exception_lib.dir/src/http_error.cpp.o
[ 13%] Building CXX object src/exception/CMakeFiles/exception_lib.dir/src/invalid_file_type.cpp.o
[ 17%] Building CXX object src/exception/CMakeFiles/exception_lib.dir/src/invalid_input.cpp.o
[ 21%] Building CXX object src/exception/CMakeFiles/exception_lib.dir/src/no_files_given.cpp.o
[ 26%] Building CXX object src/exception/CMakeFiles/exception_lib.dir/src/root_excluded.cpp.o
[ 30%] Linking CXX static library libexception_lib.a
[ 30%] Built target exception_lib
Scanning dependencies of target python_lib
[ 34%] Building CXX object src/python/CMakeFiles/python_lib.dir/src/callPython.cpp.o
[ 39%] Linking CXX static library libpython_lib.a
[ 39%] Built target python_lib
Scanning dependencies of target typeCheck_lib
[ 43%] Building CXX object src/typeCheck/CMakeFiles/typeCheck_lib.dir/src/isMusicFile.cpp.o
[ 47%] Linking CXX static library libtypeCheck_lib.a
[ 47%] Built target typeCheck_lib
Scanning dependencies of target flag_lib
[ 52%] Building CXX object src/flag/CMakeFiles/flag_lib.dir/src/flagExtract.cpp.o
[ 56%] Linking CXX static library libflag_lib.a
[ 56%] Built target flag_lib
Scanning dependencies of target inputHandler_lib
[ 60%] Building CXX object src/inputHandler/CMakeFiles/inputHandler_lib.dir/src/InputHandler.cpp.o
[ 65%] Linking CXX static library libinputHandler_lib.a
[ 65%] Built target inputHandler_lib
Scanning dependencies of target song_lib
[ 69%] Building CXX object src/song/CMakeFiles/song_lib.dir/src/selectSongs.cpp.o
[ 73%] Building CXX object src/song/CMakeFiles/song_lib.dir/src/Song.cpp.o
[ 78%] Building CXX object src/song/CMakeFiles/song_lib.dir/src/SongReader.cpp.o
[ 82%] Linking CXX static library libsong_lib.a
[ 82%] Built target song_lib
Scanning dependencies of target scraper_lib
[ 86%] Building CXX object src/scraper/CMakeFiles/scraper_lib.dir/src/scrapeLyrics.cpp.o
[ 91%] Linking CXX static library libscraper_lib.a
[ 91%] Built target scraper_lib
Scanning dependencies of target lyricScraper
[ 95%] Building CXX object CMakeFiles/lyricScraper.dir/src/lyricScraper/src/lyricScraper.cpp.o
[100%] Linking CXX executable lyricScraper
[100%] Built target lyricScraper
```

## Usage

```
lyricScraper [-options] files... [-x files...]\n";
    -x: Excludes files following this flag from being scraped
```

## Examples 

### 

./lyricScraper 

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

